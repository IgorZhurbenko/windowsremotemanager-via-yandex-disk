﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsRemoteManager
{
    enum ExecutiveStatus
    {
        Working = 1,
        Unreachable,
        Finished
    }
}

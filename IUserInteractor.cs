﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsRemoteManager.YandexDisk
{
    interface IUserInteractor
    {
        public Command RecordNewCommand();
        public string RecordNewInstructions();
    }
}

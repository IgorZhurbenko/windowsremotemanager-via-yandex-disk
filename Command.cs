﻿using System;
using System.Collections.Generic;

namespace WindowsRemoteManager
{
    class Command
    {
        public string ID;
        public List<string> Instructions;
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsRemoteManager.YandexDisk;

namespace WindowsRemoteManager
{
    class YandexDiskCommunicator : ICommunicator
    {
        private YandexDiskManager yandexDiskManager;
        public string AttachedExecutiveID { get; set; }

        public YandexDiskCommunicator(string yandexDiskToken, string yandexDiskBaseFolder, string computerBasePath = null)
        {
            //this.AttachedExecutiveID = attachedExecutiveID;
            this.yandexDiskManager = new YandexDiskManager(yandexDiskToken, yandexDiskBaseFolder, computerBasePath);
        }
        
        public bool CheckConnection()
        {
            return this.yandexDiskManager.CheckConnection();
        }

        public IEnumerable<Command> GetCommands()
        {
            List<Command> Commands = new List<Command>();
            var Structure = this.yandexDiskManager.GetFileStructure();

            IEnumerable<Command> commands = Structure.Where(file => file.Name.EndsWith($".{ConstantValues.FileExtensions.CommandInstructions}"))
                .Select(file => new Command()
                {
                    ID = file.Name.Split(".")[0]
                });

            var getInstructionsTasks = new List<Task>();

            foreach (var command in commands)
            {
                var task = new Task(() => { command.Instructions = this.GetMessage($"{command.ID}.{ConstantValues.FileExtensions.CommandInstructions}").Split('\n').ToList(); });
                task.Start();
                getInstructionsTasks.Add(task);
            }
            Task.WaitAll(getInstructionsTasks.ToArray());
            return commands;
        }

        private string GetMessage(string yandexDiskMessageName)
        {
            return this.yandexDiskManager.ReadFileFromYandexDisk(yandexDiskMessageName);
        }

        public Command GetCommand(string CommandID)
        {
            throw new NotImplementedException();
        }

        public ExecutiveStatus GetExecutiveStatus(string ExecutiveID = null)
        {
            throw new NotImplementedException();
        }

        public CommandExecutionResult GetCommandExecutionResult(string commandID)
        {
            return new CommandExecutionResult()
            {
                GivenCommandID = commandID,
                Output = GetMessage($@"{commandID}.{ConstantValues.FileExtensions.CommandExecutionResult}")
            };
        }

        public bool SendCommand(Command command)
        {
            throw new NotImplementedException();
        }

        public void SendCommandExecutionResult(CommandExecutionResult commandExecutionResult)
        {
            this.yandexDiskManager.UploadFileWithContent(commandExecutionResult.GivenCommandID + ".res", commandExecutionResult.Output);
        }

        public void ReportStatus(string statusName)
        {
            string activeMessageTitle = $"{statusName}_{DateTime.UtcNow.ToString().Replace(':', 'z')}.{ConstantValues.FileExtensions.Status}";

            var fileStructure = this.yandexDiskManager.GetFileStructure();
            
            List<Task> TaskList = new List<Task>();

            foreach (var file in fileStructure)
            {
                if (file.Name.EndsWith("." + ConstantValues.FileExtensions.Status))
                {
                    Task task = new Task(() => this.yandexDiskManager.DeleteFile(file.Name));
                    task.Start();
                    TaskList.Add(task);
                }
            }
            this.yandexDiskManager.UploadFileWithContent(activeMessageTitle);
            Task.WaitAll(TaskList.ToArray());            
        }

        private void ReportActiveStatusRepeatedly(int IntervalInSeconds, ref string logMessage)
        {
            try { ReportStatus(ConstantValues.ManagerStatuses.Active); }
            catch (Exception ex)
            {
                logMessage += $"\n{ConstantValues.LogMessages.CouldNotSendStatus}\nError: {ex.Message}\n\n";
                Thread.Sleep(1000);
                ReportStatus(ConstantValues.ManagerStatuses.Active);
            }

            Thread.Sleep(IntervalInSeconds * 1000);

            ReportActiveStatusRepeatedly(IntervalInSeconds, ref logMessage);
        }

        public void DeleteMessage(string messageTitle)
        {
            
        }

        public void RegisterExecutive(string executiveID)
        {
            this.yandexDiskManager.CreateFolder(executiveID);
            this.yandexDiskManager.YandexDiskBaseFolder += $"/{executiveID}";
        }

        public List<ExecutiveInfo> GetAllExecutives()
        {
            List<YandexDiskFileModel> str = this.yandexDiskManager.GetFileStructure();

            List<string> ExecutivesList = new List<string>();

            List<Task<string>> GetMessagesTasks = new List<Task<string>>();

            foreach (var Executive in str)
            {
                if ((Executive.Name.Length == 12) && (Executive.Type.ToLower() != "file"))
                {
                    Task<string> task = new Task<string>(

                        () => GetMessage(Executive.Name + "/" + "Info") + "|"
                        + GetExecutiveStatus(Executive.Name)

                    );
                    task.Start();
                    GetMessagesTasks.Add(task);
                }
            }

            Task.WaitAll(GetMessagesTasks.ToArray());

            foreach (Task<string> task in GetMessagesTasks)
            {
                ExecutivesList.Add(task.Result);
            }
            return ExecutivesList;
        }

    }
}

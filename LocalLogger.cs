﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace WindowsRemoteManager.YandexDisk
{
    class LocalLogger : ILocalLogger
    {
        public string LogFilePath { get; set; }
        public bool DisplayOnConsole { get; set; }

        public LocalLogger(string logFilePath, bool displayOnConsole)
        {
            this.LogFilePath = logFilePath;
            this.DisplayOnConsole = displayOnConsole;
        }

        public void Log(string Text)
        {
            string messageToWrite = DateTime.Now.ToString() + ": " + Text;
            try { File.AppendAllText(this.LogFilePath, messageToWrite); }
            catch (IOException)
            {
                System.Threading.Thread.Sleep(500);
                Log(Text);
            }
            if (DisplayOnConsole)
            {
                Console.WriteLine(DateTime.Now.ToString() + ": " + Text);
            }
        }

        public void LogEmptyLine()
        {
            try { File.AppendAllText(this.LogFilePath, "\n"); }
            catch { }
            Console.WriteLine();
        }

    }
}

﻿using System;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using System.Diagnostics;
using WindowsRemoteManager.YandexDisk;

namespace WindowsRemoteManager
{
    abstract class WindowsRemoteManagerGeneral
    {
        public ICommunicator Communicator;
        public ILocalLogger Logger;
        public ILocalCacheService LocalCacheService;
        public int RequestsIntervalInMilliseconds;
        

        public WindowsRemoteManagerGeneral(
            ICommunicator communicator, 
            ILocalLogger logger, 
            ILocalCacheService cacheService
            )
        {
            this.Communicator = communicator;
            this.LocalCacheService = cacheService;
            this.Logger = logger;
        }

    }
}


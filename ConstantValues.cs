﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsRemoteManager.YandexDisk
{
    static class ConstantValues
    {
        public static class ManagerStatuses
        {
            public static string Active = "Active";
        }

        public static class LogMessages
        {
            public static string CouldNotSendStatus = "Could not report status";
        }

        public static class FileExtensions
        {
            public const string CommandExecutionResult = "res";
            public const string CommandInstructions = "com";
            public const string Status = "stat";
        }

        public static class TimeCriticalValues
        {
            public const int MillisecondsBeforeConsideredInactive = 30;
        }
    }
}
